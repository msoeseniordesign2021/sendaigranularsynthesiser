# defines
TARGETIP=10.0.0.18

# make the target
source ~/arm-sendai-linux-gnueabihf_sdk-buildroot/environment-setup 
make -C ./Builds/SendaiHardware CONFIG=Debug

# kill gdbserver on target
ssh root@$TARGETIP killall gdbserver

# kill SendaiGranularSynthesiser
ssh root@$TARGETIP killall SendaiGranularSynthesiser
# remove old executable on target
ssh root@$TARGETIP rm /usr/bin/SendaiGranularSynthesiser
# copy over new executable
scp ./Builds/LinuxMakefile/build/SendaiGranularSynthesiser root@$TARGETIP:/usr/bin
# copy over source
scp -r ./Source root@$TARGETIP:/DefaultConfig
scp -r ./sendaiplugin root@$TARGETIP:/DefaultConfig


# start gdb on target (IS ONE LONG COMMAND)
ssh -n -f root@$TARGETIP "sh -c 'cd /DefaultConfig; DISPLAY=:0 nohup gdbserver localhost:9091 SendaiGranularSynthesiser > /dev/null 2>&1 &'"
