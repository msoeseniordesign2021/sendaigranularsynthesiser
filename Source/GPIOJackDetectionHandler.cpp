/*
  ==============================================================================

    GPIOJackDetectionHandler.cpp
    Created: 6 Feb 2022 5:19:18pm
    Author:  Danny

  ==============================================================================
*/
#ifdef SENDAI_HARDWARE
#include "GPIOJackDetectionHandler.h"


GPIOJackDetectionHandler::GPIOJackDetectionHandler() :
    headphonesGpio(17, GPIO::Edge::BOTH, [&](GPIO::Value value) {handleHpValue(value); }),
    trsGpio(16, GPIO::Edge::BOTH, [&](GPIO::Value value) {handleAuxValue(value); }),
    xlrGpio(12, GPIO::Edge::BOTH, [&](GPIO::Value value) {handleAuxValue(value); })
{
    handleHpValue(headphonesGpio.getValue());
    auto auxValue = trsGpio.getValue() == GPIO::Value::HIGH && xlrGpio.getValue() == GPIO::Value::HIGH ? GPIO::Value::HIGH : GPIO::Value::LOW;
    handleAuxValue(auxValue);
}


void GPIOJackDetectionHandler::handleHpValue(const GPIO::Value& value)
{
    if (value == GPIO::Value::HIGH)
    {
        Logger::writeToLog("HP Plugged in");
        WM8960Manager::getInstance().set_output_headphones();

    }
    else
    {
        Logger::writeToLog("HP Unplugged");
        WM8960Manager::getInstance().set_output_speaker();
    }
}


void GPIOJackDetectionHandler::handleAuxValue(const GPIO::Value& value)
{
    if (value == GPIO::Value::HIGH)
    {
        Logger::writeToLog("Aux Plugged in");
        WM8960Manager::getInstance().set_input_aux();
    }
    else
    {
        Logger::writeToLog("Aux Unplugged");
        WM8960Manager::getInstance().set_input_mic();
    }
}
#endif
