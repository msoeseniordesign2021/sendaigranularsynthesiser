#pragma once
#ifdef SENDAI_HARDWARE
#include <alsa/asoundlib.h>
#include <string>
class WM8960Manager
{
public:
    WM8960Manager(WM8960Manager const &) = delete;
    void operator=(WM8960Manager const &) = delete;

    static WM8960Manager &getInstance()
    {
        static WM8960Manager instance; // Guaranteed to be destroyed.
                                       // Instantiated on first use.
        return instance;
    }

    bool set_output_headphones()
    {
        if (!open_card())
        {
            return false;
        }

        snd_ctl_elem_id_alloca(&id);
        snd_ctl_elem_value_alloca(&value);
        snd_ctl_elem_info_alloca(&info);

        bool success = headphones_on();
        snd_ctl_elem_id_clear(id);
        success = success && speakers_off();
        close_card();
        return success;
    }

    bool set_output_speaker()
    {
        if (!open_card())
        {
            return false;
        }
        snd_ctl_elem_id_alloca(&id);
        snd_ctl_elem_value_alloca(&value);
        snd_ctl_elem_info_alloca(&info);

        bool success = speakers_on();
        snd_ctl_elem_id_clear(id);
        success = success && headphones_off();

        close_card();

        return success;
    }

    bool set_input_mic()
    {
        if (!open_card())
        {
            return false;
        }
        snd_ctl_elem_id_alloca(&id);
        snd_ctl_elem_value_alloca(&value);
        snd_ctl_elem_info_alloca(&info);

        bool success = mic_on();
        snd_ctl_elem_id_clear(id);
        success = success && aux_off();

        close_card();
        return success;
    }

    bool set_input_aux()
    {
        if (!open_card())
        {
            return false;
        }
        snd_ctl_elem_id_alloca(&id);
        snd_ctl_elem_value_alloca(&value);
        snd_ctl_elem_info_alloca(&info);

        bool success = aux_on();
        snd_ctl_elem_id_clear(id);
        success = success && mic_off();

        close_card();
        return success;
    }
    int err;
    snd_ctl_t *handle;
    snd_ctl_elem_id_t *id;
    snd_ctl_elem_value_t *value;
    snd_ctl_elem_info_t *info;

private:
    WM8960Manager() : handle(nullptr), id(nullptr), value(nullptr), info(nullptr)
    {
    }

    bool mic_on()
    {
        return use_mic() && mono_input_on();
    }

    bool mic_off()
    {
        return use_mic() && mono_input_off();
    }

    bool aux_on()
    {
        return use_aux() && mono_input_on();
    }

    bool aux_off()
    {
        return use_aux() && mono_input_off();
    }

    bool speakers_on()
    {
        return use_spekaers() && stereo_output_on();
    }

    bool speakers_off()
    {
        return use_spekaers() && stereo_output_off();
    }

    bool headphones_on()
    {
        // printf("try turn on headphones\n");
        return use_headphones() && stereo_output_on();
    }
    bool headphones_off()
    {
        return use_headphones() && stereo_output_off();
    }

    bool use_mic()
    {
        return use_contol("Left Input Boost Mixer LINPUT2 Volume");
    }

    bool use_aux()
    {
        return use_contol("Left Input Boost Mixer LINPUT3 Volume");
    }

    bool use_headphones()
    {
        return use_contol("Headphone Playback Volume");
    }

    bool use_spekaers()
    {
        return use_contol("Speaker Playback Volume");
    }

    bool stereo_output_on()
    {
        // printf("Try turn on stereo output\n");
        snd_ctl_elem_value_set_integer(value, 0, 120);
        snd_ctl_elem_value_set_integer(value, 1, 120);
        return write_control();
    }

    bool stereo_output_off()
    {
        // printf("Try turn off stereo output\n");
        snd_ctl_elem_value_set_integer(value, 0, 0);
        snd_ctl_elem_value_set_integer(value, 1, 0);
        return write_control();
    }

    bool mono_input_on()
    {
        snd_ctl_elem_value_set_integer(value, 0, 5);
        return write_control();
    }

    bool mono_input_off()
    {
        snd_ctl_elem_value_set_integer(value, 0, 0);
        return write_control();
    }

    bool write_control()
    {
        if (err = snd_ctl_elem_write(handle, value) < 0)
        {
            fprintf(stderr, "Control element write error: %s\n",
                    snd_strerror(err));
            return false;
        }
        return true;
    }

    bool open_card()
    {
        if ((err = snd_ctl_open(&handle, "hw:0", 0)) < 0)
        {
            fprintf(stderr, "Card open error: %s\n", snd_strerror(err));
            return false;
        }
        return true;
    }

    bool close_card()
    {
        return snd_ctl_close(handle) == 0;
    }

    bool use_contol(const std::string &name)
    {
        // printf("Try to change to control %s\n", name.c_str());
        snd_ctl_elem_id_set_interface(id, SND_CTL_ELEM_IFACE_MIXER);
        snd_ctl_elem_id_set_name(id, name.c_str());

        if (lookup_id(id, handle))
        {
            snd_ctl_elem_value_set_id(value, id);
            return true;
        }
        return false;
    }

    bool lookup_id(snd_ctl_elem_id_t *id, snd_ctl_t *handle)
    {
        snd_ctl_elem_info_set_id(info, id);
        if (snd_ctl_elem_info(handle, info) < 0)
        {
            fprintf(stderr, "Cannot find the given element from card\n");
            return false;
        }
        snd_ctl_elem_info_get_id(info, id);
        return true;
    }
};
#endif
