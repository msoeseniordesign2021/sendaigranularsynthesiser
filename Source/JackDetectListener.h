/*
  ==============================================================================

    JackDetectListener.h
    Created: 7 Feb 2022 8:24:34am
    Author:  Danny

  ==============================================================================
*/

#pragma once

class JackDetectListener
{
    enum PluggedInState
    {
        PluggedIn,
        Unplugged,
        Unknown
    };

    virtual void headphoneStateChanged(PluggedInState state) {};
    virtual void auxStateChanged(PluggedInState state) {};

};