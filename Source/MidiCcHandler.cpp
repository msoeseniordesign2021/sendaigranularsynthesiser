/*
  ==============================================================================

    MidiCcHandler.cpp
    Created: 18 Jan 2022 2:53:45pm
    Author:  Danny

  ==============================================================================
*/

#include "MidiCcHandler.h"
#include "../sendaiplugin/Source/GranularSynthesiserSound.h"
#include "StandalonePluginHolder.h"

enum MessageHandlingType {
    Relative2SCompParamUpdate,
    Boolean
};

constexpr auto MIDI_CC_GRAIN_LOCATION = 20;
constexpr auto MIDI_CC_GRAIN_DURATION = 21;
constexpr auto MIDI_CC_SPRAY = 22;
constexpr auto MIDI_CC_NUM_GRAINS = 23;
constexpr auto MIDI_CC_PROG_CHANGE = 24;
constexpr auto MIDI_CC_RECORD = 25;
constexpr auto MIDI_CC_SAVE = 26;
constexpr auto MIDI_CC_CURSOR = 27;
constexpr auto MIDI_CC_SELECT = 28;
constexpr auto MIDI_CC_GPIO_TOGGLE = 30;

MidiCcHandler::MidiCcHandler(StandalonePluginHolder& holder) : pluginHolder(holder)
{

}

void MidiCcHandler::updateRelativeFloatParamWithScale(String name, int value, double scale)
{
    auto param = pluginHolder.processor.valueState.getParameter(name);
    param->setValueNotifyingHost(param->getValue() + value * scale);
}

void MidiCcHandler::updateRelativeIntParamWithScale(String name, int value, int scale)
{
    auto param = (AudioParameterInt *)pluginHolder.processor.valueState.getParameter(name);
    param->setValueNotifyingHost(param->convertTo0to1(param->get() + value * scale));
}

void MidiCcHandler::handleIncomingMidiMessage(MidiInput* source, const MidiMessage& message)
{
    if (!message.isController()) return;


    MessageManager::callAsync([=]() {handleMessageInternal(message); });
}

void MidiCcHandler::handleMessageInternal(const MidiMessage message)
{
    auto val = signExtend7bit2sComp(message.getControllerValue());
    bool valTruth = val > 0;

    switch (message.getControllerNumber())
    {
    case MIDI_CC_GRAIN_LOCATION:
        updateRelativeFloatParamWithScale(GranularSynthesiserSound::CENTROID_SAMPLE_ID, val, 0.01);
        return;
    case MIDI_CC_GRAIN_DURATION:
        updateRelativeFloatParamWithScale(GranularSynthesiserSound::GRAIN_DURATION_ID, val, 0.01);
        return;
    case MIDI_CC_SPRAY:
        updateRelativeFloatParamWithScale(GranularSynthesiserSound::RANDOM_OFFSET_ID, val, 0.01);
        return;
    case MIDI_CC_NUM_GRAINS:
        updateRelativeIntParamWithScale(GranularSynthesiserSound::CLOUD_SIZE_ID, val, 1);
        return;
    case MIDI_CC_PROG_CHANGE:
        if (valTruth)
        {
            pluginHolder.progUp();
        }
        else
        {
            pluginHolder.progDown();
        }
        return;
    case MIDI_CC_RECORD:
        pluginHolder.toggleRecording();
        return;
    case MIDI_CC_SAVE:
        pluginHolder.saveProgram();
        return;
    case MIDI_CC_CURSOR:
        //not yet used
        return;
    case MIDI_CC_SELECT:
        //not yet used
        return;
    default:
        return;
    }

}

//shove a 7-bit 2s compliment number into a signed integer
int MidiCcHandler::signExtend7bit2sComp(int val)
{
    //sign extend 1s to the left if negative, else return unchanged
    return val & 1 << 6 ? val | ~((1 << 7) - 1) : val;
}
