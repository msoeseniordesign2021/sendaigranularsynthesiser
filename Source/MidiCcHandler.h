/*
  ==============================================================================

    MidiCcHandler.h
    Created: 18 Jan 2022 2:53:45pm
    Author:  Danny

  ==============================================================================
*/

#pragma once

#include "JuceHeader.h"
#include "../sendaiplugin/Source/PluginEditor.h"
#include "GPIO.h"

class StandalonePluginHolder;

class MidiCcHandler : public MidiInputCallback
{
public:

    MidiCcHandler(StandalonePluginHolder& pluginHolder);

    void handleIncomingMidiMessage(MidiInput* source, const MidiMessage& message) override;

private:

    StandalonePluginHolder& pluginHolder;

    void updateRelativeIntParamWithScale(String name, int value, int scale);
    void updateRelativeFloatParamWithScale(String name, int value, double scale);


    int signExtend7bit2sComp(int val);

    void handleMessageInternal(MidiMessage message);

};
