/*
  ==============================================================================

    MainContentComponent.cpp
    Created: 14 Jan 2022 3:42:31pm
    Author:  Danny

  ==============================================================================
*/

#include "MainContentComponent.h"
#include "../sendaiplugin/Source/GranularSynthesiserSound.h"

//==============================================================================
MainContentComponent::MainContentComponent(StandalonePluginHolder& p) :
    pluginHolder(p),
    granularVisualizer(p.processor.getVisualizer())
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize(480, 320);

    auto& processorValues = p.processor.valueState;

    processorValues.addParameterListener(GranularSynthesiserSound::CLOUD_SIZE_ID, this);
    p.addProgramChangeListener(this);

    addAndMakeVisible(granularVisualizer);


    addAndMakeVisible(programNum);
    addAndMakeVisible(numGrains);
}

MainContentComponent::~MainContentComponent()
{
}

void MainContentComponent::programChanged(int newProgram)
{
    programNum.setText(String(newProgram), NotificationType::dontSendNotification);
}

void MainContentComponent::parameterChanged(const String& parameterID, float newValue)
{
    if (parameterID == GranularSynthesiserSound::CLOUD_SIZE_ID)
    {
        numGrains.setText("# Grains: " + String((int)newValue), NotificationType::dontSendNotification);
    }
}

//void MainContentComponent::selectSampleClicked()
//{
//    fileChooser.launchAsync(FileBrowserComponent::canSelectFiles, [&](const FileChooser& chooser)
//        {
//            auto chosen = chooser.getResult();
//            if (chosen.exists())
//            {
//                pluginHolder.processor.setSampledSound(chosen);
//            }
//        });
//}

//void MainContentComponent::recordClicked()
//{
//    pluginHolder.toggleRecording();
//    if (pluginHolder.processor.isRecording())
//    {
//        recordButton.setButtonText("Record");
//    }
//    else
//    {
//        recordButton.setButtonText("Stop Recording");
//    }
//}

//==============================================================================
void MainContentComponent::paint(juce::Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll(getLookAndFeel().findColour(juce::ResizableWindow::backgroundColourId));

    g.setColour(juce::Colours::white);
    g.setFont(15.0f);
}

void MainContentComponent::resized()
{
    programNum.setBounds(3, 3, 40, 10);


    int waveHeight = 200;

    granularVisualizer.setBounds(20, getHeight() / 2 - waveHeight/2, getWidth() - 40, waveHeight);

    numGrains.setBounds(3, 300, 80, 10);
}
