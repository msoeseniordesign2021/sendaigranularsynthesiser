/*
  ==============================================================================

    MainContentComponent.h
    Created: 14 Jan 2022 3:40:47pm
    Author:  Danny

  ==============================================================================
*/

#pragma once

#include "JuceHeader.h"
#include "../sendaiplugin/Source/PluginProcessor.h"
#include "StandalonePluginHolder.h"
#include "ProgramChangeListener.h"

class MainContentComponent : public Component, public ProgramChangeListener, public AudioProcessorValueTreeState::Listener,
    private ComponentListener
{
public:
    MainContentComponent(StandalonePluginHolder&);
    ~MainContentComponent() override;


    void programChanged(int newProgram) override;
    void parameterChanged(const String& parameterID, float newValue) override;

    void paint(juce::Graphics&) override;
    void resized() override;

private:

    StandalonePluginHolder& pluginHolder;

    GranularVisualizer& granularVisualizer;

    Label programNum;
    Label numGrains;

    int maxYValue = 0; //<! Maximum Y value of Components being Drawn to the Screen

    void selectSampleClicked();
    void recordClicked();

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(MainContentComponent)
};