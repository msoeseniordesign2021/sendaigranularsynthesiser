/*
  ==============================================================================

    StandaloneFilterWindow.h
    Created: 13 Jan 2022 8:55:08am
    Author:  Danny

  ==============================================================================
*/

#pragma once

#include "JuceHeader.h"
#include "StandalonePluginHolder.h"
#include "MainContentComponent.h"
#include "GPIOJackDetectionHandler.h"

//==============================================================================
/**
    A class that can be used to run a simple standalone application containing your filter.

    Just create one of these objects in your JUCEApplicationBase::initialise() method, and
    let it do its work. It will create your filter object using the same createPluginFilter() function
    that the other plugin wrappers use.

    @tags{Audio}
*/
class StandaloneFilterWindow    : public DocumentWindow
{
public:

    //==============================================================================
    /** Creates a window with a given title and colour.
        The settings object can be a PropertySet that the class should use to
        store its settings (it can also be null). If takeOwnershipOfSettings is
        true, then the settings object will be owned and deleted by this object.
    */
    StandaloneFilterWindow (const String& title,
                            Colour backgroundColour)
        : DocumentWindow (title, backgroundColour,0)
    {
        pluginHolder = std::make_unique<StandalonePluginHolder>(File::getCurrentWorkingDirectory());
        setContentOwned (new MainContentComponent (*pluginHolder), true);

#ifdef SENDAI_HARDWARE
        setFullScreen(true);
#endif
        pluginHolder->loadLastProgram();

        setUsingNativeTitleBar(false);
        setTitleBarHeight(0);
        
    }

    ~StandaloneFilterWindow() override
    {
        pluginHolder->stopPlaying();
        clearContentComponent();
        pluginHolder = nullptr;
    }

    //==============================================================================
    AudioProcessor* getAudioProcessor() const noexcept      { return &pluginHolder->processor; }
    AudioDeviceManager& getDeviceManager() const noexcept   { return pluginHolder->deviceManager; }

    //==============================================================================
    void closeButtonPressed() override
    {
        JUCEApplicationBase::quit();
    }

    void resized() override
    {
        DocumentWindow::resized();
    }

    virtual StandalonePluginHolder* getPluginHolder()    { return pluginHolder.get(); }

    std::unique_ptr<StandalonePluginHolder> pluginHolder;

private:
#ifdef SENDAI_HARDWARE
    GPIOJackDetectionHandler jackDetectionHandler;
#endif

    //==============================================================================

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (StandaloneFilterWindow)
};
