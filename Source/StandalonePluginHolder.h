/*
  ==============================================================================

    StandalonePluginHolder.h
    Created: 13 Jan 2022 8:54:59am
    Author:  Danny

  ==============================================================================
*/

#pragma once

#include "JuceHeader.h"
#include "MidiCcHandler.h"
#include "../sendaiplugin/Source/PluginEditor.h"
#include "ProgramChangeListener.h"

//==============================================================================
/**
    An object that creates and plays a standalone instance of an AudioProcessor.

    The object will create your processor using the same createPluginFilter()
    function that the other plugin wrappers use, and will run it through the
    computer's audio/MIDI devices using AudioDeviceManager and AudioProcessorPlayer.

    @tags{Audio}
*/
class StandalonePluginHolder : Timer
{
public:
    //==============================================================================

    StandalonePluginHolder (File workingDirectory) : 
        workingDirectory(workingDirectory), 
        midiHandler(*this), 
        currentProgram(0), 
        samplesDirectory(workingDirectory.getChildFile("./samples")),
        programDirectory(workingDirectory.getChildFile("./programs")),
        lastProgramFile(workingDirectory.getChildFile(".latestProgram")),
        tmpRecordingFile(samplesDirectory.getChildFile(".tmp.wav")),
        processor(samplesDirectory)
    {
        createPlugin();
        setupAudioDevices();

        auto newSetup = deviceManager.getAudioDeviceSetup();
        Logger::writeToLog("\nPost buffer size: ");
        Logger::writeToLog(String(newSetup.bufferSize));

        jassert(workingDirectory.isDirectory());

        timerCallback();
        startPlaying();

        startTimer(5000); //check for new MIDI devices every 5 seconds
    }


    void timerCallback() override
    {
        //enable all MIDI inputs
        for (auto& device : MidiInput::getAvailableDevices())
        {
            if (!deviceManager.isMidiInputDeviceEnabled(device.identifier))
            {
                Logger::writeToLog("\nEnabling Midi Device - Name: " + device.name + " ID: " + device.identifier);
                deviceManager.setMidiInputDeviceEnabled(device.identifier, true);
            }
        }
    }


    ~StandalonePluginHolder()
    {
        deletePlugin();
        shutDownAudioDevices();
    }

    //==============================================================================
    virtual void createPlugin()
    {
        processor.disableNonMainBuses();
        processor.setRateAndBufferSizeDetails (48000, 512);
    }

    virtual void deletePlugin()
    {
        stopPlaying();
    }

    void loadLastProgram()
    {
        if (lastProgramFile.existsAsFile())
        {
            StringArray arr;
            lastProgramFile.readLines(arr);
            if (arr.size() > 0)
            {
                currentProgram = arr[0].getIntValue();
            }
        }

        loadProgram(currentProgram);
    }


    //==============================================================================
    void startPlaying()
    {
        player.setProcessor (&processor);
    }

    void stopPlaying()
    {
        player.setProcessor (nullptr);
    }
    
    void progUp()
    {
        if (currentProgram == 127) return;
        loadProgram(++currentProgram);
    }

    void progDown()
    {
        if (currentProgram == 0) return;
        loadProgram(--currentProgram);
    }


    void saveProgram()
    {

        auto programFile = programDirectory.getChildFile(String(currentProgram));
        savePluginState(programFile);
        //todo handle recorded sample
    }

    void toggleRecording()
    {
        if (processor.isRecording())
        {
            recordingIsNew = true;
            processor.stop();
            processor.setSampledSound(tmpRecordingFile);
        }
        else
        {
            processor.startRecording(tmpRecordingFile);
        }
    }


    void loadProgram(int number)
    {
        recordingIsNew = false;
        Logger::writeToLog("Loading program " + String(number));
        auto programFile = programDirectory.getChildFile(String(number));
        reloadPluginState(programFile);
        lastProgramFile.replaceWithText(String(number));

        for (auto listener : programChangeListeners)
        {
            listener->programChanged(currentProgram);
        }
    }

    //==============================================================================
    void savePluginState(File fileToWrite)
    {
        if (recordingIsNew)
        {
            auto now = Time::getCurrentTime();
            auto newFile = samplesDirectory.getNonexistentChildFile(now.formatted("%Y-%m-%d_%H_%M_%S"), ".wav", false);
            if (tmpRecordingFile.copyFileTo(newFile))
            {
                processor.setSampledSound(newFile);
                recordingIsNew = false;
            }
            else
            {
                //alert!!
                jassertfalse;
            }

        }
        processor.getStateInformationXml()->writeTo(fileToWrite);
    }



    void reloadPluginState(File fileToRead)
    {
        jassert(fileToRead.existsAsFile());
        if (fileToRead.existsAsFile())
        {
            Logger::writeToLog("Found file...");            
            processor.setStateInformationXml(*parseXML(fileToRead));
        }
        else
        {
            Logger::writeToLog("Didn't find file");
        }

    }


    void addProgramChangeListener(ProgramChangeListener * listener)
    {
        programChangeListeners.push_back(listener);
    }

    //==============================================================================

    File workingDirectory;
    File samplesDirectory;
    File programDirectory;
    File lastProgramFile;
    File tmpRecordingFile;

    SendaiPluginAudioProcessor processor;
    AudioDeviceManager deviceManager;
    AudioProcessorPlayer player;
    MidiCcHandler midiHandler;
    
private:
    std::vector<ProgramChangeListener *> programChangeListeners;
    bool recordingIsNew;
    int currentProgram;
    //==============================================================================
    void setupAudioDevices ()
    {
        deviceManager.addAudioCallback (&player);
        deviceManager.addMidiInputDeviceCallback ({}, &player);
        deviceManager.addMidiInputDeviceCallback({}, &midiHandler);
        
        auto setup = new AudioDeviceManager::AudioDeviceSetup();
        setup->sampleRate = 48000;
        setup->bufferSize = 64;
        deviceManager.initialise(2, 2, nullptr, true, {}, setup);
    }

    void shutDownAudioDevices()
    {
        deviceManager.removeMidiInputDeviceCallback ({}, &player);
        deviceManager.removeMidiInputCallback({}, &midiHandler);
        deviceManager.removeAudioCallback (&player);
    }

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (StandalonePluginHolder)
};
