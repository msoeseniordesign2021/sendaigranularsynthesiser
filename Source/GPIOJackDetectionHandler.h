/*
  ==============================================================================

    GPIOJackDetectionHandler.h
    Created: 6 Feb 2022 5:19:18pm
    Author:  Danny

  ==============================================================================
*/

#pragma once
#ifdef SENDAI_HARDWARE
#include "GPIO.h"
#include "WM8960Manager.h"
#include "JuceHeader.h"

class GPIOJackDetectionHandler

{
public:
    GPIOJackDetectionHandler();

private:

    void handleHpValue(const GPIO::Value& value);

    void handleAuxValue(const GPIO::Value& value);

    GPIO headphonesGpio;
    GPIO trsGpio;
    GPIO xlrGpio;
};
#endif
