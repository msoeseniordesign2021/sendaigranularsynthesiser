/*
  ==============================================================================

    ProgramChangeListener.h
    Created: 7 Feb 2022 8:23:58am
    Author:  Danny

  ==============================================================================
*/

#pragma once

class ProgramChangeListener
{
public:
    virtual void programChanged(int newProgram) = 0;
};
