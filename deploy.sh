# defines
TARGETIP=10.0.0.18

# make the target
source ~/arm-sendai-linux-gnueabihf_sdk-buildroot/environment-setup 
make -C ./Builds/SendaiHardware CONFIG=Release

ssh root@$TARGETIP /etc/init.d/S32main stop
ssh root@$TARGETIP 'echo 13 >/sys/class/gpio/unexport'
ssh root@$TARGETIP 'echo 26 >/sys/class/gpio/unexport'
ssh root@$TARGETIP 'echo 17 >/sys/class/gpio/unexport'
ssh root@$TARGETIP 'echo 16 >/sys/class/gpio/unexport'
ssh root@$TARGETIP 'echo 12 >/sys/class/gpio/unexport'



# copy over new executable
scp ./Builds/SendaiHardware/build/SendaiGranularSynthesiser root@$TARGETIP:/usr/bin


ssh root@$TARGETIP /etc/init.d/S32main start


# copy over source
# scp -r ./Source root@$TARGETIP:/DefaultConfig
# scp -r ./sendaiplugin root@$TARGETIP:/DefaultConfig


# start gdb on target (IS ONE LONG COMMAND)
# ssh -n -f root@$TARGETIP "sh -c 'cd /DefaultConfig; DISPLAY=:0 nohup gdbserver localhost:9091 SendaiGranularSynthesiser > /dev/null 2>&1 &'"
